v {xschem version=3.4.4 file_version=1.2
}
G {}
K {}
V {}
S {}
E {}
N 210 -310 210 -290 {
lab=VDD}
N 100 -260 100 -110 {
lab=in}
N 100 -260 160 -260 {
lab=in}
N 260 -260 310 -260 {
lab=out}
N 310 -260 310 -110 {
lab=out}
N 100 -110 180 -110 {
lab=in}
N 240 -110 310 -110 {
lab=out}
N 210 -90 210 -80 {
lab=VSS}
N 210 -140 210 -130 {
lab=VDD}
C {devices/iopin.sym} 60 -280 2 0 {name=p4 lab=out}
C {devices/iopin.sym} 60 -330 2 0 {name=p5 lab=VDD}
C {devices/iopin.sym} 60 -310 2 0 {name=p6 lab=VSS}
C {inverter2_gen/inverter2_templates_xschem/inverter2/inverter2.sym} 190 -90 0 0 {name=inv
}
C {resistor_array_gen/resistor_array_templates_xschem/resistor_array/resistor_array.sym} 150 -220 0 0 {name=Res
spiceprefix=X
}
C {devices/lab_pin.sym} 210 -310 1 0 {name=l2 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 310 -170 2 0 {name=p1 sig_type=std_logic lab=out}
C {devices/lab_pin.sym} 100 -180 0 0 {name=p2 sig_type=std_logic lab=in}
C {devices/lab_pin.sym} 210 -80 3 0 {name=p3 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 210 -140 2 0 {name=p7 sig_type=std_logic lab=VDD}

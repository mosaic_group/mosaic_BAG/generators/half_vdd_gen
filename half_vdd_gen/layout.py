import copy
from typing import *

from bag.layout.routing import TrackID
from bag.layout.template import TemplateBase

from sal.routing_grid import RoutingGridHelper
from sal.via import ViaDirection

from inverter2_gen.layout import inverter2
from resistor_array_gen.layout import resistor_array
from .params import half_vdd_layout_params


class layout(TemplateBase):
    """A Half VDD voltage generator including inverters and resistors.

    Parameters
    ----------
    temp_db : :class:`bag.layout.template.TemplateDB`
           the template database.
    lib_name : str
       the layout library name.
    params : dict[str, any]
       the parameter values.
    used_names : set[str]
       a set of already used cell names.
    **kwargs :
       dictionary of optional parameters.  See documentation of
       :class:`bag.layout.template.TemplateBase` for details.
    """
    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
        self._sch_params = None
    
    @classmethod
    def get_params_info(cls) -> Dict[str, str]:
        """
        Returns a dictionary containing parameter descriptions.

        Override this method to return a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : dict[str, str]
           dictionary from parameter name to description.
        """
        return dict(
            params='half_vdd_layout_params parameters object',
        )

    def draw_layout(self):
        """Draw the layout."""

        # make copies of given dictionaries to avoid modifying external data.
        params: half_vdd_layout_params = self.params['params'].copy()
        inv_params = params.inverter_params
        res_array_params = params.res_array_params

        # disable pins in subcells
        inv_params.show_pins = False
        res_array_params.show_pins = False

        # create layout masters for subcells we will add later
        inv_master = self.new_template(temp_cls=inverter2, params=dict(params=inv_params))
        res_array_master = self.new_template(temp_cls=resistor_array, params=dict(params=res_array_params))

        # add subcell instances
        inv_inst = self.add_instance(inv_master, 'inv', loc=(0, 0), unit_mode=True)
        x1 = inv_inst.bound_box.right_unit
        Res_inst = self.add_instance(res_array_master, 'Res', loc=(x1 + 192, 0), unit_mode=True)

        # get input/output wires from Subcells
        inv_I_warr = inv_inst.get_all_port_pins('in')[0]
        inv_O_warr = inv_inst.get_all_port_pins('out')[0]

        Res_M_warr = Res_inst.get_all_port_pins('M')[0]
        Res_P_warr = Res_inst.get_all_port_pins('P')[0]

        # get VDD/VSS wires from Subcells
        Res_VDD_warr = Res_inst.get_all_port_pins('VDD')[0]
        Res_VDD1_warr = Res_inst.get_all_port_pins('VDD')[1]

        inv_VDD_warr = inv_inst.get_all_port_pins('VDD')[0]
        inv_VSS_warr = inv_inst.get_all_port_pins('VSS')[0]

        # get layer IDs
        hm_layer = 4
        vm_layer = hm_layer + 1
        top_layer = vm_layer + 1
        grid = self.grid
        pitch_vm = self.grid.get_track_pitch(vm_layer, unit_mode=True)

        # get vertical TrackIDs
        top_w = self.grid.get_track_width(top_layer, 1, unit_mode=True)
        vm_w = self.grid.get_min_track_width(vm_layer, top_w=top_w, unit_mode=True)     # Set Track width

        # Connect resistor using M1 and M2
        Res_M_warr1 = Res_M_warr
        Res_P_warr1 = Res_P_warr

        # get vertical TrackIDs
        Res_VDD_l_tid = TrackID(vm_layer, self.grid.coord_to_nearest_track(vm_layer, Res_VDD_warr.lower),
                                width=vm_w)
        Res_M_u_tid = TrackID(vm_layer, self.grid.coord_to_nearest_track(vm_layer, Res_M_warr1.upper) + 2,
                              width=2 * vm_w)
        Res_M_l_tid = TrackID(vm_layer, self.grid.coord_to_nearest_track(vm_layer, Res_M_warr1.lower) - 2,
                              width=2 * vm_w)

        grid_helper = RoutingGridHelper(template_base=self,
                                        unit_mode=True,
                                        layer_id=vm_layer,  # unused here
                                        track_width=1)  # unused here

        # connect VDD of each block to vertical M5
        inv_I_Res_M = self.connect_to_tracks([
            inv_I_warr,
            grid_helper.connect_level_up(
                warr_id=grid_helper.connect_level_up(warr_id=Res_M_warr1,
                                                     via_dir=ViaDirection.BOTTOM,
                                                     step=0.0),
                via_dir=ViaDirection.BOTTOM,
                step=0.0
            )],
            Res_M_u_tid
        )
        inv_O_Res_P = self.connect_to_tracks([
            inv_O_warr,
            grid_helper.connect_level_up(
                warr_id=grid_helper.connect_level_up(warr_id=Res_P_warr1,
                                                     via_dir=ViaDirection.BOTTOM,
                                                     step=0.0),
                via_dir=ViaDirection.BOTTOM,
                step=0.0
            )],
            Res_M_l_tid
        )
        VDD = self.connect_to_tracks([Res_VDD_warr, Res_VDD1_warr, inv_VDD_warr], Res_VDD_l_tid)

        # Set Bounding Box and size of top block
        tot_box = inv_inst.bound_box.merge(Res_inst.bound_box)
        self.set_size_from_bound_box(vm_layer, tot_box, round_up=True)

        # add pins on wires
        self.add_pin('VDD', [Res_VDD_warr, inv_VDD_warr], show=params.show_pins)
        self.add_pin('VSS', [inv_VSS_warr], show=params.show_pins)
        # re-export pins on subcells.
        self.reexport(inv_inst.get_port('out'), show=params.show_pins)

        # compute schematic parameters.
        self._sch_params = dict(
            inv_params=inv_master.sch_params,
            res_array_params=res_array_master.sch_params,
        )

    @property
    def sch_params(self) -> Dict[str, Any]:
        return self._sch_params


class half_vdd(layout):
    """
    Class to be used as template in higher level layouts
    """
    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)

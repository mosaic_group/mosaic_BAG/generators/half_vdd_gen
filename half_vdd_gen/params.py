#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from dataclasses import dataclass
from typing import *

from sal.params_base import *

from inverter2_gen.params import inverter2_layout_params
from resistor_array_gen.params import resistor_array_layout_params


@dataclass
class half_vdd_layout_params(LayoutParamsBase):
    """
    Parameter class for half_vdd_gen

    Args:
    ----
    inverter_params : inverter2_layout_params
        Parameters for inverter2 sub-generators

    res_array_params : resistor_array_layout_params
        Parameters for resistor_array sub-generators

    guard_ring_nf : int
        Width of guard ring

    show_pins : bool
        True to create pin labels
    """

    inverter_params: inverter2_layout_params
    res_array_params: resistor_array_layout_params
    guard_ring_nf: int
    show_pins: bool

    @classmethod
    def finfet_defaults(cls, min_lch: float) -> half_vdd_layout_params:
        return half_vdd_layout_params(
            inverter_params=inverter2_layout_params.finfet_defaults(min_lch),
            res_array_params=resistor_array_layout_params.finfet_defaults(min_lch),
            guard_ring_nf=0,
            show_pins=True,
        )

    @classmethod
    def planar_defaults(cls, min_lch: float) -> half_vdd_layout_params:
        return half_vdd_layout_params(
            inverter_params=inverter2_layout_params.planar_defaults(min_lch),
            res_array_params=resistor_array_layout_params.planar_defaults(min_lch),
            guard_ring_nf=2,
            show_pins=True,
        )


@dataclass
class half_vdd_params(GeneratorParamsBase):
    layout_parameters: half_vdd_layout_params
    measurement_parameters: List[MeasurementParamsBase]

    @classmethod
    def defaults(cls, min_lch: float) -> half_vdd_params:
        return half_vdd_params(
            layout_parameters=half_vdd_layout_params.defaults(min_lch=min_lch),
            measurement_parameters=[]
        )
